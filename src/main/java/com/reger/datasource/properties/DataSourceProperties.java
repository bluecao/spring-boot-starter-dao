package com.reger.datasource.properties;

import java.util.LinkedList;
import java.util.List;

import io.shardingjdbc.core.api.algorithm.masterslave.MasterSlaveLoadBalanceAlgorithmType;

public class DataSourceProperties {
	
	/**
	 * 主库配置对象，不可以为空
	 */
	private DruidProperties master;
	
	/**
	 * 多个丛库的配置信息，可以为空,如果为空表示没有主从配置
	 */
	private List<DruidProperties> slaves=new LinkedList<DruidProperties>();
	
	/**
	 * 主从数据库负载均衡算法类型。
	 */
    private MasterSlaveLoadBalanceAlgorithmType loadBalanceAlgorithmType;
    
    /**
     * 主从数据库负载均衡算法类名
     */
    private String loadBalanceAlgorithmClassName;
    
	public MasterSlaveLoadBalanceAlgorithmType getLoadBalanceAlgorithmType() {
		return loadBalanceAlgorithmType;
	}

	public void setLoadBalanceAlgorithmType(MasterSlaveLoadBalanceAlgorithmType loadBalanceAlgorithmType) {
		this.loadBalanceAlgorithmType = loadBalanceAlgorithmType;
	}

	public String getLoadBalanceAlgorithmClassName() {
		return loadBalanceAlgorithmClassName;
	}

	public void setLoadBalanceAlgorithmClassName(String loadBalanceAlgorithmClassName) {
		this.loadBalanceAlgorithmClassName = loadBalanceAlgorithmClassName;
	}

	public DruidProperties getMaster() {
		return master;
	}

	public void setMaster(DruidProperties master) {
		this.master = master;
	}

	public List<DruidProperties> getSlaves() {
		return slaves;
	}

	public void setSlaves(List<DruidProperties> slaves) {
		this.slaves = slaves;
	}

}
