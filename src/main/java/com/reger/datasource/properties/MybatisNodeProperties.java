package com.reger.datasource.properties;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import com.reger.datasource.core.Order;

import tk.mybatis.mapper.code.Style;

public class MybatisNodeProperties {
	/**
	 * mybatis mapper接口文件的配置位置，该值不可以为空
	 */
	private String basePackage;
	/**
	 * mybatis mapper 的xml配置文件的位置 ，该值最好不要为空，
	 */
	private String mapperPackage;
	/**
	 * model类所在的包
	 */
	private String typeAliasesPackage;
	/**
	 * mybatis生成主键的顺序，如果要在insert into 数据前获得主键，配置为 BEFORE，否则配置为 AFTER
	 * </br> 默认值为 BEFORE
	 */
	private Order order = Order.BEFORE;
	
	/**
	 * mybatis通用mapper中列字段默认转化方式，
	 * normal:原值,camelhump:驼峰转下划线,uppercase:转换为大写, lowercase:转换为小写,camelhumpAndUppercase:驼峰转下划线大写形式,camelhumpAndLowercase:驼峰转下划线小写形式
	 */
	private Style style=null;
	
	/**
	 * mybatis配置参数
	 */
	private Properties properties;
	/**
	 * 是否是默认的DB对象， 
	 * </br默认值  false，
	 * </br>最多只有一个为true，
	 * </br>如果配置多个只有第一个生效,如果一个也不配置通用第一个生效
	 */
	private boolean primary;
	
	/**
	 * 分库数据源配置(没有分库数据源配置，同时没有分片配置时，只有主从配置时将使用主从配置)
	 */
	private Map<String,DataSourceProperties> dataSources;
	
	/**
	 * 分片配置(没有分库配置，同时没有分片配置时，只有主从配置时将使用主从配置)
	 */
	private ShardingProperties sharding;
    
    /**
     * 属性配置(可选)
     * sql.show: 是否开启SQL显示，默认值: false
     * executor.size: 工作线程数量，默认值: CPU核数
     */
    private Properties props = new Properties();
    
    /**
     * 
     */
    private Map<String, Object> configMap = new ConcurrentHashMap<>();
	
	public ShardingProperties getSharding() {
		return sharding;
	}

	public void setSharding(ShardingProperties sharding) {
		this.sharding = sharding;
	}

	public Map<String, Object> getConfigMap() {
		return configMap;
	}

	public void setConfigMap(Map<String, Object> configMap) {
		this.configMap = configMap;
	}

	public Properties getProps() {
		return props;
	}

	public void setProps(Properties props) {
		this.props = props;
	}

	public Map<String, DataSourceProperties> getDataSources() {
		return dataSources;
	}

	public void setDataSources(Map<String, DataSourceProperties> dataSources) {
		this.dataSources = dataSources;
	}

	public Style getStyle() {
		return style;
	}

	public void setStyle(Style style) {
		this.style = style;
	}

	public boolean isPrimary() {
		return primary;
	}

	public void setPrimary(boolean primary) {
		this.primary = primary;
	}

 

	public String getBasePackage() {
		return basePackage;
	}

	public void setBasePackage(String basePackage) {
		this.basePackage = basePackage;
	}

	public String getMapperPackage() {
		return mapperPackage;
	}

	public void setMapperPackage(String mapperPackage) {
		this.mapperPackage = mapperPackage;
	}

	public String getTypeAliasesPackage() {
		return typeAliasesPackage;
	}

	public void setTypeAliasesPackage(String typeAliasesPackage) {
		this.typeAliasesPackage = typeAliasesPackage;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}
	
}