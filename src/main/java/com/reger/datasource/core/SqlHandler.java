package com.reger.datasource.core;

public interface SqlHandler {
	
	String handler(String sql,Object ... args);
	
}
